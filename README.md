# Guía de Hoteles ! ![Status badge] (https://img.shields.io/badge/status-in%20progress-yellow)

Encuentra los hoteles mas icónicos y encontrá cual puede ser el próximo destino para unas vacaciones !!!

## 🛠 Objetivo

Plasmar los conceptos generales de desarrollo web del lado cliente, metodologías de trabajo y herramientas. Implementación de un diseño responsive, grillas, y componentes CSS y Javascript de Bootstrap. 

Incorporar preprocesadores de CSS, Less y Sass. También desarrollo con conceptos básicos de Node.js y NPM para gestionar el sitio web. 

## Aclaraciones

Este proyecto aun esta en desarrollo y tiene el fin de poder plasmar los distintos conocimientos adquiridos.

## 🚀 Getting Started

1. Clona el repositorio
1. Ve a la carpeta del proyecto ```cd gui-hoteles```
1. Instala las dependencias ```npm install```
1. Corre el ambiente local ```npm run dev```